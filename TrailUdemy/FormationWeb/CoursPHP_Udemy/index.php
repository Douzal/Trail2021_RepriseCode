<?php declare(strict_types=1);
    session_start();
    setcookie('prenom', 'Aldous', time() + 3600*24*30);
    $_SESSION['nom'] = "Maitre Doudouze";
    $_SESSION['prenom'] = "Al 1er";
    $_SESSION['ville'] = "le Mans";
    $_SESSION['medicament'] = "Memorax !";
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- used for Bootstrap -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP</title>

        <!-- BOOTSTRAP - CSS part -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <!-- BOOTSTRAP ICONS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
        
        <!-- dedicated js -->
        <!-- <link rel="stylesheet" href="css/CVscss.scss" /> -->
        
        <!-- font from google api -->
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="use-credentials" />
        <link href="https://fonts.googleapis.com/css2?family=Anton&family=Crete+Round&family=Lato:wght@100&family=Limelight&display=swap" rel="stylesheet" />

    </head>
    <body>
        
		<!-- <h1>Ceci est ma page en <?php echo 'php' ?></h1> -->

        <!-- menu ! -->
        <div class="menu">
            <?php include 'menu.php'?>
        </div>

        <h1>Index</h1>
        
        <?php
            $joursSemaine = array('lundi', 'mardi');       
            // echo $joursSemaine;     
            // var_dump($joursSemaine[1]);

            class Voiture {
                function __construct() {
                    $this -> couleur = "bleue";
                }
            }

            $voiture = new Voiture();
            // var_dump($voiture);
            $x = null;
            // var_dump($x);

            $x = 'yo les plows';
            // var_dump(strlen($x));
            // echo str_word_count($x);

            $a = 'bonjour';
            $b = 'bonsoir';
            $c = 'bonjour, ou bonsoir, je ne sais plus ...';
            // echo str_replace($a, $b, $c);

            // les constantes : 
            define('name', 'GROOT');
            // echo 'je s\'appelle ' . name;

            $num1 = '6';
            $num2 = '20';

            // var_dump($num2 / $num1);
            // echo $num2 / $num1;
            // echo ' 20/6 : '.$num2/$num1;
            // echo ' 20%6 : '.$num2%$num1;
            // echo ' le reste de la div de 20 par 6 est '. ($num2/$num1 - $num2%$num1)

            // $col = 'couleur';
            $existe = $bl ?? $b2
                        ?? $col
                        ?? 'non';
            // echo $existe;

            // operateur spaceShip
            $x = 30;
            $y = 20;
            // echo $x <=> $y; // 0 si identiques, -1 si x < y, 1 si x > y

            // echo (++$x);

            // $test = $x > 30 and $y > 10;
            $test = (true xor true);
            // var_dump('test 1 : ' . $test);
            ?><br><?php
            // var_dump($test);
            // ?><br><?php

            $test = (false xor false);
            // var_dump('test 2 : ' . $test);
            ?><br><?php
            // var_dump($test);
            
            function add(int $x, float $y):int {
                $z = $x + $y;
                // var_dump ($z);
                return $z;
            }
            ?><br><?php
            // $res = add(2, 3.5);

            # commentaire php.
            $arr = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
            $count = count($arr);
            // echo 'le tableau contient '.$count.' valeurs : ';
            for($i =0;$i<$count;$i++) {
                // echo "<br>  $arr[$i]";
            }

            foreach($arr as $var) {
                // echo '<br />'.$var;
            }

            // echo $arr[1];
            

            $arrAssociatif = array("aldous" => 32, "Laura" => 30);
            foreach ($arrAssociatif as $prenom => $age) {
                // echo "$prenom a $age ans !";
            };

            class Etudiant {

                public $etudie = true;
                public $prenom;
                public $age;
                public $notes;

                // constructeur
                function __construct($prenom, $age, $notes) {
                    $this -> prenom = $prenom;
                    $this -> age = $age;
                    $this -> notes = $notes;
                }
                
                // function
                public function sePresente() {
                    if($this -> etudie) {
                        // echo $etudiant;
                        // echo "Je m'appelle $this -> prenom, et j'ai $this -> age ans.<br />";

                        foreach($this->notes as $matiere => $note) {
                            echo "J'ai obtenu la note de $note/20 en $matiere.<br />";
                        }
                    }
                }
                
            }

            // instance Aldous
            $notesAldous = array("math" => $y, "informatique" => 17, "Baudelaire" => 14);
            $Aldous = new Etudiant('Aldous', '31', $notesAldous);
            // echo $Aldous.age;
            $Aldous->sePresente();


            // variables GLOBALES
            $x = 5;
            $y = 18;
            function learnGlobals () {
                echo $GLOBALS['y'];
            }
            // learnGlobals();

            // var_dump($_SERVER);
            // echo $_SERVER['SERVER_NAME']; // $_SERVER contient des variables serveur
            // var_dump($_ENV);

        ?>

        <!-- <form action="presentation.php" method="post">
            <label for="nom">Nom :</label><input type="text" id="nom" name="nom"><br>
            <label for="ville">Ville : </label><input type="text" id="ville" name="ville"><br>
            <input type="submit" />
        </form> -->

        <!-- LIEN PRESENTATION -->
        <!-- <a href="../presentation.php?nom=Alexis&ville=Niort" >Présentation</a> -->

        <!-- COOCKIES -->
        <?php
            if(isset($_COOKIE['prenom'])) {
                echo "cookie : ". $_COOKIE['prenom'];
            } else {
                echo "Rien de cuisiné par ici...";
            }
        ?>

        <!-- jQuery -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <!-- AXIOS-->
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script> -->

        <!-- POPPER (for dropdown menus in bootstrap)-->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->

        <!-- BOOTSTRAP - Js part - JavaScript Bundle + Popper (no use of Popper CDN then) -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
        
        <!-- SASS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sass.js/0.9.2/sass.min.js"></script>
        
        <script type="text/javascript" src="script/CVjs.js"></script>
    </body>
</html>