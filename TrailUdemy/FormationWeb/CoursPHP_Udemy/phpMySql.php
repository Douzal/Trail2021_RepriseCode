<?php
    declare(strict_types=1);
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- used for Bootstrap -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP & SQL</title>

        <!-- BOOTSTRAP - CSS part -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <!-- BOOTSTRAP ICONS -->
        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css"> -->
        
        <!-- dedicated css -->
        <!-- <link rel="stylesheet" href="css/CVscss.scss" /> -->
        
        <!-- font from google api -->
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Anton&family=Crete+Round&family=Lato:wght@100&family=Limelight&display=swap" rel="stylesheet" />

    </head>
    <body>
        

        <!-- menu ! -->
        <!-- <div class="menu">
            <?php include 'menu.php'?>
        </div> -->

        <?php

            try {
                $database = new PDO(
                    'mysql:host=localhost;dbname=store;charset=utf8;', 'root', '');        
                $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $e) {
                die('Error : ' . $e->getMessage());
            }

            $req = 'SELECT first_name, last_name FROM customers LIMIT 100';
            $res = $database ->query($req);

            $pren = "Johnny";
            $nom = 'Halliday';

            $req = 'INSERT INTO customers(first_name, last_name) VALUES (?,?)';
            $sql = $database->prepare($req);

            $arr = array($pren, $nom);
            $sql->execute($arr);

            // $param = 3; //'PDO::FETCH_ASSOC';
            // $resFetch = $res->fetch(); //(PDO::FETCH_ASSOC);
            // var_dump($resFetch);
            // $resFetchAll = $res->fetchAll(PDO::FETCH_ASSOC);
            // var_dump($resFetchAll);

            // while($row = $res->fetch()) {
            //     echo $row['first_name']."&nbsp-&nbsp";
            //     echo $row['last_name']."</br>";
            // }

            // $results = $database->query('SELECT first_name, last_name FROM customers');
            // $row2 = $results->fetch();
            // $i=1;
            // while($row2 = $results->fetch()) {
                // echo 'Client '.$i.' : '.$row2['first_name'].' '.$row2['last_name'].'</br>';
                // $row2 = $results->fetch();
                // $i++;
            // }

            // $req = 'INSERT INTO customers(first_name, last_name) VALUES (\'Claude\', \'Benchichon\')';
            // $database->query($req);

        ?>
        

        <!-- jQuery -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <!-- AXIOS-->
        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script> -->

        <!-- POPPER (for dropdown menus in bootstrap)-->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->

        <!-- BOOTSTRAP - Js part - JavaScript Bundle + Popper (no use of Popper CDN then) -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
        
        <!-- SASS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sass.js/0.9.2/sass.min.js"></script>
        
        <script type="text/javascript" src="script/CVjs.js"></script>
    </body>
</html>