<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Burger Code</title>

    <!-- used for Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- BOOTSTRAP - CSS part -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- BOOTSTRAP ICONS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- favicon burger -->
    <link rel="shortcut icon" href="../img/icon_faviconBurger.png" type="image/png">
    
    <!-- dedicated CSS -->
    <link href="../style/style.css" rel="stylesheet"/>
</head>

<body>
    <h1 class="h1exception text-logo"><span class="glyphicon glyphicon-cutlery"></span> Miam Code <span class="glyphicon glyphicon-fire"></span></h1>
    <div class="container admin">
        <!-- le CSS :not(.h1exception, .h1exception>span) ne fonctionnant pas, j'ai sorti le h1 de la div .container mais ça m'énerve  -->
        <!-- <h1 class="h1exception text-logo"><span class="glyphicon glyphicon-cutlery"></span> Miam Code <span class="glyphicon glyphicon-fire"></span></h1> -->
        <div class="row">
            <h1><strong>liste des items </strong><a href="insert.php" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-plus"> Ajouter</span></a></h1>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>nom</th>
                    <th>description</th>
                    <th>prix</th>
                    <th>catégorie</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
                <!-- php : get items -->
                <?php
                    require 'database.php';
                    $db = Database::connect();
                    $req = "
                        SELECT items.id, items.name, items.description, items.price, items.category, categories.id, categories.name AS category
                        FROM items LEFT JOIN categories ON items.category = categories.id
                        ORDER BY items.id DESC
                    ";
                	$statement = $db->query($req);
                    while($item = $statement->fetch()) {
                        echo '<tr>';
                        echo '<td>'.$item['name'].'</td>';
                        echo '<td>'.$item['description'].'</td>';
                        echo '<td>'.$item['price'].'</td>';
                        echo '<td>'.$item['category'].'</td>';
                        echo '<td width="300px">';
                                echo'<a class="buttons btn btn-default" href="view.php?id='.$item['id'].'"><span class="glyphicon glyphicon-eye-open"></span> Voir</a> ';
                                echo '<a class="buttons btn btn-primary" href="update.php?id='.$item['id'].'"><span class="glyphicon glyphicon-pencil"></span> Modifier</a> ';
                                echo '<a class=" buttons btn btn-danger" href="delete.php?id='.$item['id'].'"><span class="glyphicon glyphicon-remove"></span> Supprimer</a>';
                        echo '</td>';
                        echo '</tr>';
                    }
                ?>
            </tbody>
        </table>
    </div>





    <!-- PART SCRIPTS -->
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- AXIOS-->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script> -->

    <!-- POPPER (for dropdown menus in bootstrap)-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->

    <!-- BOOTSTRAP - Js part - JavaScript Bundle + Popper (no use of Popper CDN then) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>

    <!-- SASS-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sass.js/0.9.2/sass.min.js"></script> -->
    <!-- dedicated js -->
    <!-- <script type="text/javascript" src="script/CVjs.js"></script> -->
</body>

</html>