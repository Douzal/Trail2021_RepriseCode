<?php

    class Database {
        private static $dbHost          = "localhost";
        private static $dbName          = "burger_code";
        private static $dbUser          = "root";
        private static $dbUserPassword  = "";
        private static $cxn             = null;
        
        public static function connect() {
            try {
                // pourquoi ça marche pas, cette ligne ??
                // self::$cxn = new PDO("mysql:host=".self::$dbHost.";dbname=".self::$dbName.",".self::$dbUser.",".self::$dbUserPassword);
                self::$cxn = new PDO("mysql:host=".self::$dbHost.";dbname=".self::$dbName, self::$dbUser, self::$dbUserPassword);
                // $stringCxn = "mysql:host=".self::$dbHost.";dbname=".self::$dbName.",".self::$dbUser.",".self::$dbUserPassword;
                // $stringCxn = "mysql:host=localhost;dbname=burger_code,";
                // self::$cxn = new PDO($stringCxn, self::$dbUser, self::$dbUserPassword);
            } catch (PDOexception $exc) {
                // stop execution and print error
                die($exc->getMessage());
            }
            return self::$cxn;
        }

        public static function disconnect() {
            self::$cxn = null;
        }
    } 
    Database::connect();
?>