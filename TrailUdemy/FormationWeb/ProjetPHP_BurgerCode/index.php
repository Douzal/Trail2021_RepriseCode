<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Burger Code</title>

    <!-- used for Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- BOOTSTRAP - CSS part -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- BOOTSTRAP ICONS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

    
    <!-- font from google api -->
    <!-- <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Crete+Round&family=Lato:wght@100&family=Limelight&display=swap"
    rel="stylesheet" /> -->
    
    <!-- favicon burger -->
    <link rel="shortcut icon" href="./img/icon_faviconBurger.png" type="image/png">
    
    <!-- dedicated CSS -->
    <link rel="stylesheet" href="style/style.css" />
</head>

<body>
    <div class="container site">
        <!-- TITRE / LOGO -->
        <h1 class="text-logo">
            <!-- <span class="bi-shop"> ?? -->
            <span class="glyphicon glyphicon-cutlery"></span> miam burger <span class="glyphicon glyphicon-cutlery"></span>
        </h1>

        <!-- NAVBAR -->
        <nav>
            <ul class="nav nav-pills" role="tablist">
                <li role="presentation" class="nav-item"><a data-bs-toggle="pill" data-bs-target="#tab1" role="tab" class="nav-link active">menus</a></li>
                <li role="presentation" class="nav-item"><a data-bs-toggle="pill" data-bs-target="#tab2" role="tab" class="nav-link">burgers</a></li>
                <li role="presentation" class="nav-item"><a data-bs-toggle="pill" data-bs-target="#tab3" role="tab" class="nav-link">snacks</a></li>
                <li role="presentation" class="nav-item"><a data-bs-toggle="pill" data-bs-target="#tab4" role="tab" class="nav-link">salades</a></li>
                <li role="presentation" class="nav-item"><a data-bs-toggle="pill" data-bs-target="#tab5" role="tab" class="nav-link">boissons</a></li>
                <li role="presentation" class="nav-item"><a data-bs-toggle="pill" data-bs-target="#tab6" role="tab" class="nav-link">desserts</a>
                </li>
            </ul>
        </nav>

        <!-- MAIN CONTENT -->
        <div class="tab-content">
            <!-- row 1 -->
            <div class="tab-pane active" id="tab1" role="tabpanel">
                <div class="row">
                    <!-- sm6 : si je passe en small je mets juste 2 col
                    si je passe medium, 3 col -->
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="./img/m1.png" class="img-fluid"  alt="...">
                            <div class="price">8.9 €</div>
                            <div class="caption">
                                <h4>menu classique</h4>
                                <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                                <a href="#" class="btn btn-order" role="button">
                                    <!-- <span class="glyphicon glyphicon-shopping-cart"></span>-->
                                    <span class="bi-cart-fill"></span>commander
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                            <div class="img-thumbnail">
                                <img src="img/m2.png" class="img-fluid" alt="...">
                                <div class="price">9.50 €</div>
                                <div class="caption">
                                    <h4>menu Bacon</h4>
                                    <p>Sandwich: Burger, Fromage, Bacon, Salade, Tomate + Frites + Boisson</p>
                                    <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="img-thumbnail">
                                <img src="img/m3.png" class="img-fluid" alt="...">
                                <div class="price">10.90 €</div>
                                <div class="caption">
                                    <h4>Menu Big</h4>
                                    <p>Sandwich: Double Burger, Fromage, Cornichon, Salade + Frites + Boisson</p>
                                    <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="img-thumbnail">
                                <img src="img/m4.png" class="img-fluid" alt="...">
                                <div class="price">9.90 €</div>
                                <div class="caption">
                                    <h4>Menu Chicken</h4>
                                    <p>Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise + Frites + Boisson</p>
                                    <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="img-thumbnail">
                                <img src="img/m5.png" class="img-fluid" alt="...">
                                <div class="price">10.90 €</div>
                                <div class="caption">
                                    <h4>Menu Poisson</h4>
                                    <p>Sandwich: Poisson, Salade, Mayonnaise, Cornichon + Frites + Boisson</p>
                                    <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="img-thumbnail">
                                <img src="img/m6.png" class="img-fluid" alt="...">
                                <div class="price">11.90 €</div>
                                <div class="caption">
                                    <h4>Menu Double barbaque</h4>
                                    <p>Sandwich: Double Burger, Fromage, Bacon, Salade, Tomate + Frites + Boisson</p>
                                    <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

            <!-- onglet 2 : BURGERS -->
            <div class="tab-pane" id="tab2" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/b1.png" class="img-fluid" alt="...">
                            <div class="price">5.90 €</div>
                            <div class="caption">
                                <h4>Classic</h4>
                                <p>Sandwich: Burger, Salade, Tomate, Cornichon</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/b2.png" class="img-fluid" alt="...">
                            <div class="price">6.50 €</div>
                            <div class="caption">
                                <h4>Bacon</h4>
                                <p>Sandwich: Burger, Fromage, Bacon, Salade, Tomate</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/b3.png" class="img-fluid" alt="...">
                            <div class="price">6.90 €</div>
                            <div class="caption">
                                <h4>Big</h4>
                                <p>Sandwich: Double Burger, Fromage, Cornichon, Salade</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/b4.png" class="img-fluid" alt="...">
                            <div class="price">5.90 €</div>
                            <div class="caption">
                                <h4>Chicken</h4>
                                <p>Sandwich: Poulet Frit, Tomate, Salade, Mayonnaise</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/b5.png" class="img-fluid" alt="...">
                            <div class="price">6.50 €</div>
                            <div class="caption">
                                <h4>Poisson</h4>
                                <p>Sandwich: Poisson, Salade, Mayonnaise, Cornichon</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/b6.png" class="img-fluid" alt="...">
                            <div class="price">7.50 €</div>
                            <div class="caption">
                                <h4>Double Steak</h4>
                                <p>Sandwich: Double Burger, Fromage, Bacon, Salade, Tomate</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- onglet 3 : SNACKS -->
            <div class="tab-pane" id="tab3" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/s1.png" class="img-fluid" alt="...">
                            <div class="price">3.90 €</div>
                            <div class="caption">
                                <h4>frites</h4>
                                <p>pommes de terre frites</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/s2.png" class="img-fluid" alt="...">
                            <div class="price">3.40 €</div>
                            <div class="caption">
                                <h4>Onion Rings</h4>
                                <p>Rondelles d'oignon frits</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/s3.png" class="img-fluid" alt="...">
                            <div class="price">5.90 €</div>
                            <div class="caption">
                                <h4>Nuggets</h4>
                                <p>Nuggets de poulet frits</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/s4.png" class="img-fluid" alt="...">
                            <div class="price">3.50 €</div>
                            <div class="caption">
                                <h4>Nuggets Fromage</h4>
                                <p>Nuggets de fromage frits</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/s5.png" class="img-fluid" alt="...">
                            <div class="price">5.90 €</div>
                            <div class="caption">
                                <h4>Ailes de Poulet</h4>
                                <p>Ailes de poulet Barbecue</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- onglet 4 : SALADES -->
            <div class="tab-pane" id="tab4" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/sa1.png" class="img-fluid" alt="...">
                            <div class="price">8.90 €</div>
                            <div class="caption">
                                <h4>César Poulet Pané</h4>
                                <p>Poulet Pané, Salade, Tomate et la fameuse sauce César</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/sa2.png" class="img-fluid" alt="...">
                            <div class="price">8.90 €</div>
                            <div class="caption">
                                <h4>César Poulet Grillé</h4>
                                <p>Poulet Grillé, Salade, Tomate et la fameuse sauce César</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/sa3.png" class="img-fluid" alt="...">
                            <div class="price">5.90 €</div>
                            <div class="caption">
                                <h4>Salade légère</h4>
                                <p>Salade, Tomate, Concombre, Maïs et Vinaigre balsamique</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/sa4.png" class="img-fluid" alt="...">
                            <div class="price">7.90 €</div>
                            <div class="caption">
                                <h4>Poulet Pané</h4>
                                <p>Poulet Pané, Salade, Tomate et la sauce de votre choix</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/sa5.png" class="img-fluid" alt="...">
                            <div class="price">7.90 €</div>
                            <div class="caption">
                                <h4>Poulet Grillé</h4>
                                <p>Poulet Grillé, Salade, Tomate et la sauce de votre choix</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- onglet 5 : BOISSONS -->
            <div class="tab-pane" id="tab5" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/bo1.png" class="img-fluid" alt="...">
                            <div class="price">1.90 €</div>
                            <div class="caption">
                                <h4>Coca-Cola</h4>
                                <p>Au choix: Petit, Moyen ou Grand</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/bo2.png" class="img-fluid" alt="...">
                            <div class="price">1.90 €</div>
                            <div class="caption">
                                <h4>Coca-Cola léger</h4>
                                <p>Au choix: Petit, Moyen ou Grand</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/bo3.png" class="img-fluid" alt="...">
                            <div class="price">1.90 €</div>
                            <div class="caption">
                                <h4>Coca-Cola (double) Zéro</h4>
                                <p>Au choix: Petit, Moyen ou Grand</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/bo4.png" class="img-fluid" alt="...">
                            <div class="price">1.90 €</div>
                            <div class="caption">
                                <h4>Fanta</h4>
                                <p>Au choix: Petit, Moyen ou Grand</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/bo5.png" class="img-fluid" alt="...">
                            <div class="price">1.90 €</div>
                            <div class="caption">
                                <h4>Sprite</h4>
                                <p>Au choix: Petit, Moyen ou Grand</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/bo6.png" class="img-fluid" alt="...">
                            <div class="price">1.90 €</div>
                            <div class="caption">
                                <h4>thé glacé resucré</h4>
                                <p>Au choix: Petit, Moyen ou Grand. <br> Pas mauvais, pas mauvais !</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- onglet 6 : DESSERTS -->
            <div class="tab-pane" id="tab6" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/d1.png" class="img-fluid" alt="...">
                            <div class="price">4.90 €</div>
                            <div class="caption">
                                <h4>Fondant au chocolat</h4>
                                <p>Au choix: Chocolat Blanc ou au lait</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/d2.png" class="img-fluid" alt="...">
                            <div class="price">2.90 €</div>
                            <div class="caption">
                                <h4>Muffin</h4>
                                <p>Au choix: Au fruits ou au chocolat</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/d3.png" class="img-fluid" alt="...">
                            <div class="price">2.90 €</div>
                            <div class="caption">
                                <h4>Beignet</h4>
                                <p>Au choix: Au chocolat ou à la vanille</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/d4.png" class="img-fluid" alt="...">
                            <div class="price">3.90 €</div>
                            <div class="caption">
                                <h4>Milkshake</h4>
                                <p>Au choix: Fraise, Vanille ou Chocolat</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="img-thumbnail">
                            <img src="img/d5.png" class="img-fluid" alt="...">
                            <div class="price">4.90 €</div>
                            <div class="caption">
                                <h4>Sundae</h4>
                                <p>Au choix: Fraise, Caramel ou Chocolat</p>
                                <a href="#" class="btn btn-order" role="button"><span class="bi-cart-fill"></span> Commander</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>

    </div>

    <!-- PART SCRIPTS -->
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- AXIOS-->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script> -->

    <!-- POPPER (for dropdown menus in bootstrap)-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->

    <!-- BOOTSTRAP - Js part - JavaScript Bundle + Popper (no use of Popper CDN then) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>

    <!-- SASS-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sass.js/0.9.2/sass.min.js"></script> -->
    <!-- dedicated js -->
    <!-- <script type="text/javascript" src="script/CVjs.js"></script> -->
</body>

</html>