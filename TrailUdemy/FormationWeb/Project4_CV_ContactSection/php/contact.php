<?php

    $arr = array("firstname" => "", "name" => "", "email" => "", "phone" => "", "message" => "", "firstnameErr" => "", "nameErr" => "", "emailErr" => "", "phoneErr" => "", "messageErr" => "", "isSuccess" => false);
    $emailText = "";
    $emailTo = "aldousP@outlook.fr";

    if($_SERVER['REQUEST_METHOD'] == 'POST') {

        // load entered values
        $arr["firstname"]   =   verifyInput($_POST['firstname']);
        $arr["name"]        =   verifyInput($_POST['name']);
        $arr["email"]       =   verifyInput($_POST['email']);
        $arr["phone"]       =   verifyInput($_POST['phone']);
        $arr["message"]     =   verifyInput($_POST['message']);
        $arr["isSuccess"]   =   true;

        // server validation        
        if(empty($arr["firstname"])) {
            $arr["firstnameErr"] = "Ton prénom, soit gentil.";
            $arr["isSuccess"] = false;
        } else {
            $emailText .= "Prénom : {$arr["firstname"]}\n";
        }
        
        // name error
        if(empty($arr["name"])) {
            $arr["nameErr"] = "Ton nom serait apprécié !";
            $arr["isSuccess"] = false;
        } else {
            $emailText .= "Nom : {$arr["name"]}\n";
        }

        // mail err
        if(!isEmail($arr["email"])){
            $arr["emailErr"] = "Mail, et au bon format, please !";
            $arr["isSuccess"] = false;
        } else {
            $emailText .= "Mail : {$arr["email"]}\n";
        };

        // phone err
        if(!isPhone($arr["phone"])) {
            $arr["phoneErr"] = "Attention à entrer un numéro valide (en 06-07) !";
            $arr["isSuccess"] = false;
        } else {
            $emailText .= "Tél : {$arr["phone"]}\n";
            // $emailText .= "Tél : $phone\n";

        }

        // mess err
        if(empty($arr["message"])) {
            $arr["messageErr"] = "Entre nous un gentil message :)";
            $arr["isSuccess"] = false;
        } else {
            $emailText .= "Message : \n{$arr["message"]}";
        }

        // envoyer l'email
        if($arr["isSuccess"]) {
            sendMail($emailText, $arr["firstname"], $arr["name"], $emailTo, $arr["email"]);
            // clearDatas($arr["firstname"], $arr["name"], $arr["email"], $arr["phone"], $arr["message"],
            // $arr["firstnameErr"], $arr["nameErr"], $arr["emailErr"], $arr["phoneErr"], $arr["messageErr"], $arr["isSuccess"]); // marche pas..
            // $firstname = $name = $email = $phone = $message = '';
            // $firstnameErr = $nameErr = $emailErr = $phoneErr = $messageErr = '';
            $emailText = '';
        }
        
        // RENVOYER L'ARRAY RESULT EN JSON
        echo json_encode($arr);
    }

    function sendMail($mail, $pren, $name, $receiverMail, $senderMail) {
        // $headers = "From : $pren $name <$mail>\r\nReply to : $mail";
        $headers = "From: $pren $name \nReply to : $senderMail";
        mail($receiverMail, "Un message du site !", $mail, $headers);
    }

    function clearDatas($firstname, $name, $email, $phone, $message,
    $firstnameErr, $nameErr, $emailErr, $phoneErr, $messageErr, $isSuccess) {
        $firstname = $name = $email = $phone = $message = '';
        $firstnameErr = $nameErr = $emailErr = $phoneErr = $messageErr = '';
        $isSuccess = false;
    }

    function isEmail($mail) {
        return filter_var($mail, FILTER_VALIDATE_EMAIL);
    }

    function isPhone($phone) {
        return preg_match("/^[0-9 ]*$/", $phone);
    }

    function verifyInput($var) {
        $var =  stripslashes(
                    htmlspecialchars(
                        trim($var)));

        return $var;
    }
?>