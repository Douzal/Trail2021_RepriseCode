$(function() {
    'use strict';
    console.warn('CV bootstrap');

    /* TOOLTIPS */
    // let tooltipTriggerList = Array.from(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    // var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    //     return new bootstrap.Tooltip(tooltipTriggerEl);
    // });

    /* MODALS */
    /*allow focus when opening MODALS */
    // var myModal = document.getElementById('exampleModal');
    // var myInput = document.getElementById('myInput');

    // myModal.addEventListener('shown.bs.modal', function () {
    //     myInput.focus();
    // });
    

    /* ANIMATE IMAGE */
    let img100 = $('.img100');
    img100.mouseover(function() {
        img100.stop(true, true); // allows to clear queue : https://api.jquery.com/stop/
        img100.animate({
            left: '10px',
            // width: '+=10px'
        }, 200);
    }).mouseout(function() {
        img100.stop(true, true);
        img100.animate({
            left: '-0.8px',
            // width: '-=10px'
        }, 200);
    });

    /* ANIMATE FORMATION'S BLOCKS */
    /* let animBlocks = $('.formation__block');
    console.log([animBlocks]);
    for(let block of animBlocks) {
        console.log(block);
        block.mouseover(function() {
            animBlocks.animate({
                left: '10px',
                // width: '+=10px'
            }, 200);
        }).mouseout(function() {
            animBlocks.animate({
                left: '-0.8px',
                // width: '-=10px'
            }, 200);
        });
    } */

    /* TOOLTIP BOOTSTRAP - https://getbootstrap.com/docs/4.0/components/tooltips/ */
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })

    /* CONTACT FORM - AJAX */
    let form = $('#contact-form');
    form.submit(function(e){

        // supprimer l'action par défaut de soumission du form
        e.preventDefault();

        // vider les messages d'erreur
        $('.comments').empty();

        // vider l'éventuel thank-you
        // $('.thank-you').text('');
        $('.thank-you').css('visibility', 'hidden');

        // get entered values in JSON object
        let postData = form.serialize();
        // console.info(`postData avant : ${postData}`);

        $.ajax({
            type:'POST',
            url: 'php/contact.php',
            data: postData,
            // ContentType: 'application/json',
            dataType: 'json',
            success: function(res) {
                if(res.isSuccess) {
                    console.warn('SUCCESS : \n', res);
                    // show THANKS message
                    $('.thank-you').css('visibility', 'visible');
                    
                    // reset form
                    form[0].reset();
                } else {
                    console.warn('FAILED : ', res);
                    $("#firstname + .comments").html(res.firstnameErr);
                    $("#name + .comments").html(res.nameErr);
                    $("#email + .comments").html(res.emailErr);
                    $("#phone + .comments").html(res.phoneErr);
                    $("#message + .comments").html(res.messageErr);
                }
            },
            error: function (jqxhr, textStatus, errorThrown) {
                // console.error("jqxhr : ", jqxhr, "\nerror : ", textStatus, '\nerrorThrown : ', errorThrown);
            }
        });
        
    });

})