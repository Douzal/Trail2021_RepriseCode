<?php
    $firstname = $name = $email = $phone = $message = '';
    $firstnameErr = $nameErr = $emailErr = $phoneErr = $messageErr = '';
    $isSuccess = false;
    $emailTo = "aldousP@outlook.fr";
    $emailText = "";

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // load entered values
        $firstname  =   verifyInput($_POST['firstname']);
        $name       =   verifyInput($_POST['name']);
        $email      =   verifyInput($_POST['email']);
        $phone      =   verifyInput($_POST['phone']);
        $message    =   verifyInput($_POST['message']);

        $isSuccess = true;

        // server validation        
        if(empty($firstname)) {
            $firstnameErr = "Ton prénom, soit gentil.";
            $isSuccess = false;
        } else {
            $emailText .= "Prénom : " . $firstname."\n";
        }
        
        // name error
        if(empty($name)) {
            $nameErr = "Ton nom serait apprécié !";
            $isSuccess = false;
        } else {
            $emailText .= "Nom : " . $name."\n";
        }

        // mail err
        if(!isEmail($email)){
            $emailErr = "Mail, et au bon format, please !";
            $isSuccess = false;
        } else {
            $emailText .= "Mail : " . $email."\n";
        };

        // phone err
        if(!isPhone($phone)) {
            $phoneErr = "Attention à entrer un numéro valide (en 06-07) !";
            $isSuccess = false;
        } else {
            $emailText .= "Tél : " . $phone."\n";
            // $emailText .= "Tél : $phone\n";

        }

        // mess err
        if(empty($message)) {
            $messageErr = "Entre nous un gentil message :)";
            $isSuccess = false;
        } else {
            $emailText .= "Message : \n" . $message;
        }

        // envoyer l'email
        if($isSuccess) {
            sendMail($emailText, $firstname, $name, $emailTo, $email);
            // clearDatas(); // marche pas..
            $firstname = $name = $email = $phone = $message = '';
            $firstnameErr = $nameErr = $emailErr = $phoneErr = $messageErr = '';
            $emailText = '';
        }
    }

    function sendMail($mail, $pren, $name, $receiverMail, $senderMail) {
        // $headers = "From : $pren $name <$mail>\r\nReply to : $mail";
        $headers = "From: $pren $name \nReply to : $senderMail";
        mail($receiverMail, "Un message du site !", $mail, $headers);
    }

    function clearDatas() {
        $firstname = $name = $email = $phone = $message = '';
        $firstnameErr = $nameErr = $emailErr = $phoneErr = $messageErr = '';
        $isSuccess = false;
    }

    function isEmail($mail) {
        return filter_var($mail, FILTER_VALIDATE_EMAIL);
    }

    function isPhone($phone) {
        return preg_match("/^[0-9 ]*$/", $phone);
    }

    function verifyInput($var) {
        $var =  stripslashes(
                    htmlspecialchars(
                        trim($var)));

        return $var;
    }
?>