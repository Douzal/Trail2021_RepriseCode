$(function(){
    let form = $('#contact-form');
    form.submit(function(e){

        // supprimer l'action par défaut de soumission du form
        e.preventDefault();

        // vider les messages d'erreur
        $('.comments').empty();

        // vider l'éventuel thank-you
        // $('.thank-you').text('');
        $('.thank-you').css('visibility', 'hidden');

        // get entered values in JSON object
        // console.info(`form avant : ${JSON.parse('form')}`);
        let postData = form.serialize();
        console.info(`postData avant : ${postData}`);
        
        {
            // repair string
            // if(postData.charAt(postData.length-1)=="=") {
            //     postData+="a";
            // }
            // console.info(`postData after troubleshooting : ${postData}`);
        }
        
        $.ajax({
            type:'POST',
            url: 'php/contactV2.php',
            data: postData,
            // ContentType: 'application/json',
            dataType: 'json',
            success: function(res) {
                if(res.isSuccess) {
                    console.warn('SUCCESS : \n', res);
                    // show THANKS message
                    $('.thank-you').css('visibility', 'visible');
                    
                    // reset form
                    form[0].reset();
                } else {
                    console.warn('FAILED : ', res);
                    $("#firstname + .comments").html(res.firstnameErr);
                    $("#name + .comments").html(res.nameErr);
                    $("#email + .comments").html(res.emailErr);
                    $("#phone + .comments").html(res.phoneErr);
                    $("#message + .comments").html(res.messageErr);
                }
            },
            error: function (jqxhr, textStatus, errorThrown) {
                console.error("jqxhr : ", jqxhr, "\nerror : ", textStatus, '\nerrorThrown : ', errorThrown);
            }
        });
        
    });
})

